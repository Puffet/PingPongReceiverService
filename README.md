# PingPongReceiverService Exmple Repository. 

Main page for this repository can be found at: https://gitlab.com/vandy-aad-3/PingPongReceiverService

## To clone the repository

copy one of the following URLs (Depending on your desired/configured connection method.)

for HTTPS connections use: ```https://gitlab.com/vandy-aad-3/PingPongReceiverService.git```

for an SSH connection use: ```git@gitlab.com:vandy-aad-3/PingPongReceiverService.git ```


### To browse the Code & Commits of this repository directly gitlab.com

1. visit the main page of the project

2. click the 'Repository' tab at the top(center) of the window.

   1. The 'Files' tab will lead to the Files.

   2. The 'Commits' tab will lead to the Commits.